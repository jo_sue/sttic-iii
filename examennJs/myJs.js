$(function() {
    var dialog, form, form1, row, cols;
    
   
    function addCell(valeur) {
        return "<td>" +
            valeur +
            "</td>"
    }
    //fonction qui enregistre un medecin
    function addCours() {
        //activation de la page saveMedecin.php pour l'enregistrement d'un medecin
        //a travers une requete ajax
        $.ajax({
            //la page php a activer
            url: "saveCours.php",
            //les donnees sont envoyées a saveMedecin.php avec la medthode post
            type: "POST",
            //la page saveMedecin.php retour des données json
            dataType: "json",
            //les donnees  a  mettre dans le tableau $_POST
            /*
    Exemple=un formulaire avec des champs nom prenom et age comme nom de champ
    form1 represente l'ID du formulaire
    $('#form1').serialize() retoune 
    nom=xxxxx&prenom=zzzzz&age=20
    ces données sont envoyées dans le tableau $_POST 
    */
            data: $("#form1").serialize(),
            //fonction à appeler si la page saveMedecin.php s'est executée sans erreur
            //les donnees retournees par la page saveMedecin.php est retourné dans la variable data
            success: function(data) {
                if (data.status == 'i') {
                    console.log(data)
                        //ajout d'une ligne dans le tableau des medecins de la page index.html
                    $("#courss tbody").append(
                        "<tr>" +
                        addCell($("#code").val()) +
                        addCell($("#nomCours").val()) +
                        addCell($("#jours option:selected").html()) +
                        addCell($("#heureD").val()) +
                        addCell($("#heureF").val()) +
                        addCell($("#coef").val()) +
                        addCell($("#sal option:selected").html()) +
                        addCell($("#mat option:selected").html()) +
                        "</tr>"
                    );
                } 
                //permet de fermer la fenetre de dialogue
                dialog.dialog("close");
            },
            //cette fonction est appelé si la page saveMedecin.php souleve une exeption
            error: function(err) {
                +console.log(err);
            },
        });
        //permet dee ne pas poster de formulaire
        return false;
    }
    //permet d'initialiser
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        height: 515,
        width: 510,
        modal: true,
        buttons: {
            "Enregistrer": function() {
                console.log($('#coursId').val())
                addCours()
            },
            Annuler: function() {
                dialog.dialog("close");
            },
        },
        close: function() {
            form[0].reset();
            //allFields.removeClass( "ui-state-error" );
        },
    });    

    form = dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        addUser();
    });
    //applique l'evènement click sur le bouton 'ajouter medecin'
    $("#create-cours")
        .button()
        .on("click", function() {
            $('#coursId').val('0')
            $('#code').removeAttr('readonly')
                //ouverture du dialog
            dialog.dialog("open");
        });
    //permet de recuperer les specialites a partir de la bd pour charger les select de la specialité
    $.ajax
    ({
        url: "fillSalle.php",
        type: "GET",
        dataType: "json",
        success: function(data) 
        {
            $.each(data, function(indice, sal) 
            {
                $("#sal").append
                (
                    '<option value="' +
                    sal.salleId +
                    '">' +
                    sal.libelle +
                    "</option>"
                );
            });
        },
        error: function(err) 
        {
            console.log(err);
        },
    });
    $.ajax
    ({
        url: "fillMatiere.php",
        type: "GET",
        dataType: "json",
        success: function(data) {
            $.each(data, function(indice, mat) 
            {
                $("#mat").append
                (
                    '<option value="' +
                    mat.matiereId +
                    '">' +
                    mat.libelle1 +
                    "</option>"
                );
            });
        },
        error: function(err) 
        {
            console.log(err);
        },
    });

    $.ajax({
        url: "coursListe.php",
        type: "GET",
        dataType: "json",
        success: function(data) {
            $.each(data, function(indice, cours) {
                $("#courss tbody").append(
                    "<tr>" +
                    addCell(cours.code) +
                    addCell(cours.nomCours) +
                    addCell(cours.jours) +
                    addCell(cours.heureD) +
                    addCell(cours.heureF) +
                    addCell(cours.coef) +
                    addCell(cours.libelle) +
                    addCell(cours.libelle1) +
                    
                    "</tr>"
                );
            });
        },
        error: function(err) {
            console.log(err);
        },
    });
});